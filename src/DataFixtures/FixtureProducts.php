<?php

namespace App\DataFixtures;
use app\Entity\Products;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FixtureProducts extends Fixture
{
    public function load(ObjectManager $manager): void
    {
       
        for($i =1; $i <=5; $i++)
        {
            $produit = new Products();
            $produit -> setName("Burger n°$i")
                     -> setDescription("Pain, Steak, Bacon, Salade, Cheddar")
                     -> setImage("https://picsum.photos/200/150")
                     -> setPrice("10")
                     -> setAvailable(true);
            $manager ->persist($produit);
        }
        $manager ->flush(); 
    }
            
    
}
