<?php

namespace App\DataFixtures;
use app\Entity\User;
use Doctrine\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AdminFixtures extends Fixture
{
    public function __construct( UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $hashedPassword = $this->passwordHasher->hashPassword(
            $admin,
            "1234"
        );
        $admin  ->setEmail("admin@dcd.fr")
                ->setName("Joe")
                ->setPhone("+33612345678")
                ->setPassword($hashedPassword)
                ->setRoles(['ROLE_ADMIN']);
        // $product = new Product();
        $manager->persist($admin);

        $manager->flush();
    }
}
