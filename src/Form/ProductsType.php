<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Products;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;



class ProductsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label_format'=>'Nom'])
            ->add('description',TextType::class, ['label_format'=>'Description'])
            ->add('price', TextType::class, ['label_format'=>'Prix'])
            ->add('image',FileType::class, [
                'mapped' => false,
                'label_format'=>'Image'])
            ->add('available', ChoiceType::class, [
                'choices'  => [
                    'Disponible' => true,
                    'Indisponible' => false,
                ],
            
            ])
            ;
            
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
        ]);
    }
}
