const temps = new Date();
let d = temps.getDay();
if (temps.getHours() >= 10 && temps.getHours() < 13) {
  document.getElementById("etat").innerHTML = "Ouvert";
} else if (temps.getHours() >= 18 && temps.getHours() < 22) {
  document.getElementById("etat").innerHTML = "Ouvert";
} else if (d == 7) {
  document.getElementById("etat").innerHTML = "Fermé";
} else {
  document.getElementById("etat").innerHTML = "Fermé";
  document.getElementById("etat").classList.add("close");
}
console.log(temps);